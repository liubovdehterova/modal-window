import React, {useState} from 'react';
import './App.css';
import Button from "./components/Button";
import Modal from "./components/Modal";
import {useStyleModal, useStylesBtn} from "./components/stylesComponents/stylesComponent";


function App() {
    const [isFirstModalOpen, setFirstModalOpen] = useState(false);
    const [isSecondModalOpen, setSecondModalOpen] = useState(false);

    const toggleModal = (modalType) => {
        if (modalType === 'first') {
            setFirstModalOpen((prev) => !prev);
        } else if (modalType === 'second') {
            setSecondModalOpen((prev) => !prev);
        }
    }; //Замінила дві функції на одну для відстежування стану


    const closeModal = () => {
        setFirstModalOpen(false);
        setSecondModalOpen(false)
    }
    const modalFirstActions = [
        {
            label: 'OK', onClick: () => {
                console.log('OK');
                closeModal(); //Модальне вікно закривається та в консоль виводить ОК як підтвердження що клік був саме по цій кнопці
            }
        },
        {
            label: 'Cancel', onClick: () => {
                console.log('Cancel');
                closeModal(); //Модальне вікно закривається та в консоль виводить Cancel як підтвердження що клік був саме по цій кнопці
            }
        },
    ];
    const modalSecondActions = [
        {
            label: 'Save', onClick: () => {
                console.log('Saved');
                closeModal();//Модальне вікно закривається та в консоль виводить Saved як підтвердження що клік був саме по цій кнопці
            }
        },
        {
            label: 'Undo', onClick: () => {
                console.log('Undo');
                closeModal();//Модальне вікно закривається та в консоль виводить Undo як підтвердження що клік був саме по цій кнопці
            }
        },
    ];
    return (
        <div className="App">
            <Button text="Open first modal" backgroundColor="#d44637" openModal={() => toggleModal("first")} useStylesBtn={useStylesBtn}/>
            <Button text="Open second modal" backgroundColor="#208fd9" openModal={() => toggleModal("second")} useStylesBtn={useStylesBtn} />
            <Modal isOpen={isFirstModalOpen}
                   backgroundColor="#e74c3c"
                   backgroundColorHeader="#d44637"
                   backgroundColorBtn = "#b3382c"
                   onClose={closeModal}
                   actions={modalFirstActions}
                   header="Do you want to delete this file?"
                   textInfo="Once you delete this file, it won’t be possible to undo this action."
                   textAsk="Are you sure you want to delete it?"
                   closeButton={true}
                   styleModal={useStyleModal}
            />
            <Modal isOpen={isSecondModalOpen}
                   backgroundColor="#3498db"
                   backgroundColorHeader="#208fd9"
                   backgroundColorBtn = "#1e83c7"
                   onClose={closeModal}
                   actions={modalSecondActions}
                   header="Would you like to add information?"
                   textInfo="Added information will be available to all users"
                   textAsk="Should I share this?"
                   closeButton={true}
                   styleModal={useStyleModal}
            />
        </div>
    );
}

export default App;
