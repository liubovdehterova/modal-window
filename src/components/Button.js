import React from "react";

function Button({text, backgroundColor, openModal, useStylesBtn}) {
    const classes = useStylesBtn({backgroundColor});
    return (
        <button type="button" className={classes.btn} onClick={openModal}>
            {text}
        </button>
    )
}

export default Button;