import React from "react";
import Crosse from "../images/cross-svgrepo-com.svg"


function Modal({header, closeButton, textInfo, textAsk, actions, isOpen, onClose, styleModal, backgroundColor, backgroundColorBtn, backgroundColorHeader}) {
    const classes = styleModal({backgroundColor, backgroundColorHeader, backgroundColorBtn});

    const handleClickOutside = (event) => {
        event.preventDefault();
        if (event.target.className === classes.modal) {
            onClose() //Модальне вікно закривається коли клік відбувся по сірому полупрозорому фоні модального вікна
        }
    }

    if (!isOpen) {
        return null;
    }
    return (
        <div className={classes.modal} onClick={handleClickOutside}>
            <div className={classes.modal__wrapper}>
                <header className={classes.modal__header}>
                    <h2 className={classes.modal__title}>
                        {header}
                    </h2>
                    <button type="button" className={closeButton ? classes.visible : classes.hidden} onClick={onClose}>
                        <img src={Crosse} alt="cross" className={classes.visible__img}/>
                    </button>
                </header>
                <p className={classes.modal__text}>
                    {textInfo}
                </p>
                <p className={classes.modal__text}>
                    {textAsk}
                </p>
                <div className={classes.modal__button}>
                    {actions && actions.map((action, index) => (
                        <button type="button"
                                className={classes.modal__button__item}
                                key={index}
                                onClick={action.onClick}>
                            {action.label}
                        </button>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Modal;