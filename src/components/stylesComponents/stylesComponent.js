import {createUseStyles} from 'react-jss';

export const useStylesBtn = createUseStyles({
    btn: ({backgroundColor}) => ({
        padding: '10px',
        fontSize: '16px',
        color: 'white',
        borderRadius: '5px',
        margin: '15px',
        backgroundColor: backgroundColor || '#3498db',
    }),
});
export const useStyleModal = createUseStyles({
    modal: {
        position: 'fixed',
        top: '0',
        left: '0',
        background: 'rgba(49,46,46,0.8)',
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        color: '#ffffff'
    },
    modal__wrapper: ({backgroundColor}) => ({
        background: backgroundColor || '#d44637',
        padding: '0 0 20px 0',
        width: '480px',
    }),
    modal__header: ({backgroundColorHeader}) => ({
        backgroundColor: backgroundColorHeader || '#e74c3c',
        padding: '20px',
        display: 'flex',
        justifyContent: 'space-between',
    }),
    visible: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '24px',
        height: '24px',
        padding: '0',
        border: 'none',
        background: 'none'
    },
    hidden: {
        display: 'none',
        padding: '0',
    },
    visible__img: {
        width: '15px',
        height: '15px',
    },
    modal__title: {
        margin: '0',
    },
    modal__text: {
        padding: '10px 20px 0',
        margin: '0',
        textAlign: 'center',
        fontFamily: 'Times New Roman',
        fontSize: '15px'
    },
    modal__button: {
        display: 'flex',
        justifyContent: 'center',
        margin: '20px 0 0 0'
    },
    modal__button__item: ({backgroundColorBtn}) => ({
        width: '100px',
        padding: '10px 0',
        borderRadius: '5px',
        border: 'none',
        margin: '0 10px',
        background: backgroundColorBtn || '#b3382c',
        color: '#ffffff'
    })
});
